# Bitcoin Cash

A Bitcoin cash docker image.

## Tags

- `0.24.0` ([0.24.0/GitHub](https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v24.0.0))

For release notes go to [this page](https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases).

## What is Bitcoin Cash?

[Bitcoin Cash](https://www.bitcoincash.org) is the name of open-source software which enables the use of Bitcoin Cash. It is a descendant of the Bitcoin Core and Bitcoin ABC software projects.

## Usage

### How to use this image

You must run a container with predefined environment variables.

Create `.env` file with following variables:

| Env variable                    | Description                                                                |
| ------------------------------- | -------------------------------------------------------------------------- |
| `RPC_USER_NAME`                 | User to secure the JSON-RPC api.                                           |
| `RPC_USER_PASSWORD`             | A password to secure the JSON-RPC api.                                     | 
| `PATH_TO_DATA_FOLDER`           | Full path to the blockchain data folder.                                   |

Run container: 

```bash
  docker-compose up -d
```

Stop container: 

```bash
  docker-compose down
```
